// This file is here to make Rollup bundle PDFJS in a separate file,
// just to make the build smaller & faster
// (Without this, the whole non-minified build clocks in at 2.7MiB).

import PDFJS from "pdfjs-dist";

import {} from "pdfjs-dist/build/pdf.worker.entry.js";

export default PDFJS;
