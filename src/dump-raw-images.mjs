// The following two imports are handled through public/externals-pdfjs.js
// This is done just to save time on the rebuilds.

// import PDFJS from "pdfjs-dist";		/// DEBUG!! Should be removed for non-CLI usage
// import {} from "pdfjs-dist/build/pdf.worker.entry.js";

import array from "array-range";
import { blockhashData } from "./blockhash.mjs";

function getFilteredOperations(operations) {
	// 	console.log(
	// 		array(operations.fnArray.length).map((index) => ({
	// 			opcode: operations.fnArray[index],
	// 			args: operations.argsArray[index],
	// 		}))
	// 	);

	return array(operations.fnArray.length)
		.map((index) => ({
			opcode: operations.fnArray[index],
			args: operations.argsArray[index],
		}))
		.filter((item) => hasParserForOpcode(item.opcode));
}

// `file` must be a File from the DOM File API (see https://developer.mozilla.org/en-US/docs/Web/API/File)
// Returns a promise to a PDFJS document.
function getDocument(file) {
	// Skip fetching on CLI usage
	if (typeof process !== "undefined") {
		return Promise.resolve(file);
	}

	return new Promise(function(resolve, reject) {
		const reader = new FileReader();
		reader.onload = function() {
			PDFJS.getDocument({
				data: new Uint8Array(reader.result),
				disableFontFace: true,
			}).then((doc) => resolve(doc));
		};

		reader.readAsArrayBuffer(file);
	});
}

// `file` must be a File from the DOM File API (see https://developer.mozilla.org/en-US/docs/Web/API/File)
// Returns a promise to the number of pages in the PDF.
export function getPageCount(file) {
	return getDocument(file).then((doc) => doc.numPages);
}

// `file` must be a File from the DOM File API (see https://developer.mozilla.org/en-US/docs/Web/API/File)
// `onImage`, `onPage`, `onDone` and `onError` must be callback functions
// If the aspect ratio is larger than `maxAspectRatio` (wither wider or taller), the image will be discarded
// If wither the width or height is smaller than `minSize`, the image will be discarded
// Returns a promise which resolves when done
export default function forEachImageInPDF(
	file,
	onImage,
	onPage,
	onDone,
	onError,
	maxAspectRatio = 4,
	minSize = 96
) {
	initParsers();

	return getDocument(file)
		.then((doc) =>
			doc
				.getPageLabels()
				.then((labels) => ({ doc, labels: labels ? labels : [] }))
		)
		.then(
			({ doc, labels = [] }) => {
				let queue = Promise.resolve();

				const getPageName = (pageNumber) =>
					labels.length > pageNumber ? labels[pageNumber] : "";

				array(1, doc.numPages + 1).forEach((pageNumber) => {
					queue = queue
						.then(() => onPage(pageNumber))
						.then(() => doc.getPage(pageNumber))
						.then((page) =>
							onEachPage(
								page,
								pageNumber,
								getPageName(pageNumber - 1),
								onImage,
								maxAspectRatio,
								minSize
							)
						);
				});

				queue = queue.then(() => onDone());

				return queue;
			},
			(error) => onError(`${error.name} - ${error.message}`)
		);
}

// Internal, called on each parsed page of the PDF. Just a small refactor.
// Returns a Promise that resolves when the page has been processed.
function onEachPage(page, pageNumber, pageName, onImage, maxAspectRatio, minSize) {
	return page.getOperatorList().then((ops) => {
		const pagePromises = [];

		getFilteredOperations(ops).forEach(({ opcode, args }) => {
			const [_, w, h] = args;
			if (
				w / h > maxAspectRatio ||
				h / w > maxAspectRatio ||
				h < minSize ||
				w < minSize
			) {
				// console.debug(
				// 	"Ignoring image with dimensions ",
				// 	w,
				// 	h
				// );
			} else {
				// opsQueue = opsQueue.then(() =>
				pagePromises.push(
					handleImage(page, pageNumber, pageName, args, onImage, opcode)
				);
			}
		});

		return Promise.all(pagePromises);
	});
}

// Image parsers for the different opcodes.
// Depending on the opcode, some images are canvas-compatible, and
// some images need manual byte unpacking
const parsers = {};

function initParsers() {
	// 	if (parsers[PDFJS.OPS.paintImageXObject]) {
	if (parsers[85]) {
		return;
	}

	// paintImageXObject = 85
	// 	parsers[PDFJS.OPS.paintImageXObject] = {
	parsers[85] = {
		bytes: (image) => image.data,
		toCanvas: (ctx, args, image) => handlePaintImageXObject(ctx, args, image),
		type: "ImageXObject",
	};

	// paintInlineImageXObject = 86
	// 	parsers[PDFJS.OPS.paintInlineImageXObject] = {
	parsers[86] = {
		bytes: (image) => image.data,
		toCanvas: (ctx, args, image) => handlePaintImageXObject(ctx, args, image),
		type: "InlineImageXObject",
	};

	// paintJpegXObject = 82
	// 	parsers[PDFJS.OPS.paintJpegXObject] = {
	parsers[82] = {
		bytes: (image) => getJpegBytes(image),
		toCanvas: (ctx, _, image) => ctx.drawImage(image, 0, 0),
		type: "JpegXObject",
	};
}

const knownHashes = [];

export function updateKnownHashes(newHashes) {
	newHashes.forEach((hash) => addHash(hash));
}

function hasParserForOpcode(opcode) {
	return Object(parsers).hasOwnProperty(opcode);
}

function addHash(hash) {
	if (!isHashKnown(hash)) {
		knownHashes.push(hash);
	}
}

function isHashKnown(hash) {
	return knownHashes.includes(hash);
}

// Cryptographic hash function, will depend on the platform this is running in (CLI/browser)
let cryptoHash;

import crypto from "crypto";

if (typeof process !== "undefined" && process) {
	// Assume node.js
	cryptoHash = function cryptoHash(bytes) {
		const hash = crypto.createHash("sha256");
		hash.update(bytes);
		return Promise.resolve(hash.digest("hex"));
	};
} else {
	// Assume web browser
	cryptoHash = function cryptoHash(bytes) {
		return crypto.subtle.digest("SHA-256", bytes);
	};
}

// Called whenever there is a recognised image operation in a page.
// Returns a Promise to a `Blob` with the image data.
// `pageNumber` is the page the operation was located on.
// `pageName` is the label for the page.
// `args` are the arguments for the image operation, in the form
// `[imagename, width, height]`.
// `onImage` is a callback function for what happens when the image is handled.
// `opcode` refers to the PDF operation code containing the image (since there are several possible)
function handleImage(page, pageNumber, pageName, args, onImage, opcode) {
	const [imageName, w, h] = args;

	let image;
	try {
		image = page.objs.get(imageName);
	} catch (ex) {
		console.warn("Could not fetch image, waiting a bit...");
		return new Promise((resolve, reject) =>
			setTimeout(() => {
				handleImage(page, pageNumber, pageName, args, onImage, opcode);
				resolve();
			}, 2500)
		);
	}

	if (!Object(parsers).hasOwnProperty(opcode)) {
		console.debug("Have no handler for this opcode:", opcode);
		return new Promise(function(resolve) {
			resolve();
		});
	}

	const parser = parsers[opcode];
	const bytes = Uint8ClampedArray.from(parser.bytes(image));

	return cryptoHash(bytes).then((hash) => {
		const hashStr = Array.from(new Uint8Array(hash), (byte) =>
			(byte & 0xff).toString(16).padStart(2, "0")
		).join("");

		if (isHashKnown(hashStr)) {
			console.debug("Image already used before, skipping");
			return;
		}

		addHash(hashStr);

		const phash = blockhashData(
			{ width: image.width, height: image.height, data: bytes },
			16,
			2 /*2*/
		);

		// console.debug({
		// 	phash: phash,
		// 	hash: hashStr,
		// 	name: imageName,
		// 	width: image.width,
		// 	height: image.height,
		// 	kind: image.kind,
		// });

		const canvas = document.createElement("canvas");
		canvas.height = h;
		canvas.width = w;
		const ctx = canvas.getContext("2d");

		parser.toCanvas(ctx, args, image);

		// Prepare to clip the image - detect empty rows and columns (alpha < 8),
		// then copy the canvas to a new one.
		let transBytes;
		let minx = 0,
			miny = 0,
			maxx = w,
			maxy = h;
		let keep = true;

		// Detect empty rows
		for (let y = 0; keep && y < h; y++) {
			transBytes = ctx.getImageData(0, y, w, 1).data;
			for (let x = 0; keep && x < w; x++) {
				// Transparent is alpha 0; opaque is 255. Threshold at 8 should work.
				if (transBytes[x * 4 + 3] > 8) {
					keep = false;
				}
			}
			if (keep) {
				miny++;
			}
		}
		keep = true;
		for (let y = h - 1; keep && y > 0; y--) {
			transBytes = ctx.getImageData(0, y, w, 1).data;
			for (let x = 0; keep && x < w; x++) {
				if (transBytes[x * 4 + 3] > 8) {
					keep = false;
				}
			}
			if (keep) {
				maxy--;
			}
		}
		keep = true;

		// Detect empty cols
		for (let x = 0; keep && x < w; x++) {
			transBytes = ctx.getImageData(x, 0, 1, h).data;
			for (let y = 0; keep && y < h; y++) {
				if (transBytes[y * 4 + 3] > 8) {
					keep = false;
				}
			}
			if (keep) {
				minx++;
			}
		}
		keep = true;
		for (let x = w - 1; keep && x > 0; x--) {
			transBytes = ctx.getImageData(x, 0, 1, h).data;
			for (let y = 0; keep && y < h; y++) {
				if (transBytes[y * 4 + 3] > 8) {
					keep = false;
				}
			}
			if (keep) {
				maxx--;
			}
		}
		// 		console.log('Empty borders:', minx, maxx, miny, maxy, '(', w, h, ')');

		const clipCanvas = document.createElement("canvas");

		clipCanvas.width = maxx - minx;
		clipCanvas.height = maxy - miny;

		clipCanvas
			.getContext("2d")
			.drawImage(
				canvas,
				minx,
				miny,
				maxx - minx,
				maxy - miny,
				0,
				0,
				maxx - minx,
				maxy - miny
			);
		// 			.drawImage(canvas, sx, sy, sw, sh, dx, dy, dw, dh);

		return new Promise((resolve, reject) =>
			clipCanvas.toBlob(
				(blob) =>
					onImage({
						name: imageName,
						blob,
						pageNumber,
						pageName,
						mimeType: "image/png",
						originalType: parser.type,
						hash: hashStr,
						phash,
						w,
						h,
					}).then(resolve, reject),
				"image/png"
			)
		);
	});
}

// Called whenever there is a `paintImageXObject` operation in a page.
// `ctx` is the canvas context to paint on.
// `args` are the arguments for the `paintImageXObject` operation, in the form
// `[imagename, width, height]`.
// `image` is the page object
function handlePaintImageXObject(ctx, args, image) {
	const [_, w, h] = args;

	// 'kind' marks the bit depth. See
	// https://github.com/mozilla/pdf.js/blob/b56081c5f89c0602151df149a622876bac8c21d4/src/shared/util.js#L41
	if (image.kind === 2 || image.kind === 3) {
		let imageData;

		const bytes = Uint8ClampedArray.from(image.data);

		if (image.kind === 2) {
			// 24-bit RGB
			const pixelCount = w * h;
			const rgbaBytes = new Uint8ClampedArray(pixelCount * 4);
			for (let i = 0; i < pixelCount; i++) {
				rgbaBytes.set(bytes.subarray(i * 3, i * 3 + 3), i * 4);
				rgbaBytes[i * 4 + 3] = 255;
			}
			imageData = new ImageData(rgbaBytes, w, h);
		} else if (image.kind === 3) {
			// 32-bit RGBA
			imageData = new ImageData(bytes, w, h);
		}

		ctx.putImageData(imageData, 0, 0);
	} else {
		console.warn("Unrecognized image colour depth");
	}
}

function getJpegBytes(image) {
	const canvas = document.createElement("canvas");
	const context = canvas.getContext("2d");
	canvas.width = image.width;
	canvas.height = image.height;
	context.drawImage(image, 0, 0);
	return context.getImageData(0, 0, image.width, image.height).data;
}
